// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "NetworkModule",
    platforms: [.iOS(.v15)],
    products: [
        .library(
            name: "NetworkModule",
            type: .dynamic,
            targets: ["NetworkModule"]),
    ],
    dependencies: [],
    targets: [
        .target(
            name: "NetworkModule",
            dependencies: []),
        .testTarget(
            name: "NetworkModuleTests",
            dependencies: ["NetworkModule"]),
    ]
)
