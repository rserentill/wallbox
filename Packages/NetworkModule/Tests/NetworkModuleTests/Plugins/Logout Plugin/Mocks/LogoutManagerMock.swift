//
//  LogoutManagerMock.swift
//  
//
//  Created by Roger Serentill Gené on 18/8/22.
//

@testable import NetworkModule

final class LogoutManagerMock: LogoutManagerProtocol {
  static private(set) var logoutCalled = false
  
  static var logoutStatusCode: Int = -1
  
  static func logout() {
    logoutCalled = true
  }
  
  static func reset() {
    logoutCalled = false
    logoutStatusCode = -1
  }
}
