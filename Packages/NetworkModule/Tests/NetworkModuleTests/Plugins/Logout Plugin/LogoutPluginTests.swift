//
//  LogoutPluginTests.swift
//  
//
//  Created by Roger Serentill Gené on 18/8/22.
//

import XCTest

@testable import NetworkModule

final class LogoutPluginTests: XCTestCase {
  override func tearDown() async throws {
    LogoutManagerMock.reset()
  }
  
  func test_didReceiveResponse_shouldLogout() {
    LogoutManagerMock.logoutStatusCode = 401
    let response = HTTPURLResponseMock(statusCode: 401)
    let request = URLRequestMock(url: .init(string: "https://xerinola.io")!)
    
    let sut = LogoutPlugin(manager: LogoutManagerMock.self)
    
    sut.didReceive(response: response, data: nil, from: request)
    
    XCTAssertTrue(LogoutManagerMock.logoutCalled)
  }
  
  func test_didReceiveResponse_shouldNotLogout() {
    LogoutManagerMock.logoutStatusCode = 401
    let response = HTTPURLResponseMock(statusCode: 200)
    let request = URLRequestMock(url: .init(string: "https://xerinola.io")!)
    
    let sut = LogoutPlugin(manager: LogoutManagerMock.self)
    
    sut.didReceive(response: response, data: nil, from: request)
    
    XCTAssertFalse(LogoutManagerMock.logoutCalled)
  }
}
