//
//  OAuthPluginTests.swift
//  
//
//  Created by Roger Serentill Gené on 18/8/22.
//

import XCTest

@testable import NetworkModule

final class OAuthPluginTests: XCTestCase {
  override func tearDown() async throws {
    OAuthManagerMock.reset()
  }

  func test_interceptRequest_validToken() async {
    OAuthManagerMock.token = .init(token: "123", expirationDate: Date().addingTimeInterval(300))
    
    let sut = OAuthPlugin(manager: OAuthManagerMock.self)
    
    let request = URLRequest(url: .init(string: "https://xerinola.io")!)
    
    let intercepted = await sut.intercept(request: request)
    
    XCTAssertFalse(OAuthManagerMock.requestTokenCalled)
    
    guard let header = intercepted.value(forHTTPHeaderField: "authorization") else {
      XCTFail("Authorization header not added to the request")
      return
    }
    
    XCTAssertEqual(header, "Bearer 123")
  }
  
  func test_interceptRequest_aboutToExpireToken() async {
    OAuthManagerMock.token = .init(token: "old", expirationDate: Date().addingTimeInterval(10))
    OAuthManagerMock.requestedToken = .init(token: "new", expirationDate: Date().addingTimeInterval(400))
    
    let sut = OAuthPlugin(manager: OAuthManagerMock.self)
    
    let request = URLRequest(url: .init(string: "https://xerinola.io")!)
    
    let intercepted = await sut.intercept(request: request)
    
    XCTAssertTrue(OAuthManagerMock.requestTokenCalled)
    
    guard let header = intercepted.value(forHTTPHeaderField: "authorization") else {
      XCTFail("Authorization header not added to the request")
      return
    }
    
    XCTAssertEqual(header, "Bearer new")
  }
  
  func test_interceptRequest_expiredToken() async {
    OAuthManagerMock.token = .init(token: "old", expirationDate: Date().addingTimeInterval(-10))
    OAuthManagerMock.requestedToken = .init(token: "new", expirationDate: Date().addingTimeInterval(400))
    
    let sut = OAuthPlugin(manager: OAuthManagerMock.self)
    
    let request = URLRequest(url: .init(string: "https://xerinola.io")!)
    
    let intercepted = await sut.intercept(request: request)
    
    XCTAssertTrue(OAuthManagerMock.requestTokenCalled)
    
    guard let header = intercepted.value(forHTTPHeaderField: "authorization") else {
      XCTFail("Authorization header not added to the request")
      return
    }
    
    XCTAssertEqual(header, "Bearer new")
  }
}
