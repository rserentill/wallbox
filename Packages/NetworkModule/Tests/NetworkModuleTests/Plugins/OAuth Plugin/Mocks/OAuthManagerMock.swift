//
//  OAuthManagerMock.swift
//  
//
//  Created by Roger Serentill Gené on 18/8/22.
//

@testable import NetworkModule

final class OAuthManagerMock {
  static private(set) var requestTokenCalled = false
  static var requestedToken: Token?
  
  static func reset() {
    requestTokenCalled = false
    requestedToken = nil
  }
}

// MARK: - OAuthManagerProtocol

extension OAuthManagerMock: OAuthManagerProtocol {
  static var token: Token?
  
  static func requestToken() async -> Token? {
    requestTokenCalled = true
    return requestedToken
  }
}
