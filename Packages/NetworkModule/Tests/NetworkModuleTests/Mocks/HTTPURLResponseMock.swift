//
//  HTTPURLResponseMock.swift
//  
//
//  Created by Roger Serentill Gené on 18/8/22.
//

@testable import NetworkModule

final class HTTPURLResponseMock: HTTPURLResponseProtocol {
  var statusCode: Int

  init(statusCode: Int) {
    self.statusCode = statusCode
  }
}
