//
//  URLRequestMock.swift
//  
//
//  Created by Roger Serentill Gené on 18/8/22.
//

@testable import NetworkModule

import Foundation

final class URLRequestMock: URLRequestProtocol {
  var url: URL?
  
  init(url: URL) {
    self.url = url
  }
}
