//
//  URLParameterEncoderTests.swift
//  
//
//  Created by Roger Serentill Gené on 29/8/22.
//

import XCTest

@testable import NetworkModule

final class URLParameterEncoderTests: XCTestCase {
  func test_encode() {
    let sut = URLParameterEncoder.self
    
    var request = URLRequest(url: .init(string: "https://api.xerinola.io")!)
    
    do {
      try sut.encode(request: &request, with: ["foo": "bar"])
      
      XCTAssertEqual(request.url?.query, "foo=bar")
      
      guard let header = request.value(forHTTPHeaderField: "Content-Type") else {
        XCTFail("Content-Type header not added to the request")
        return
      }
      XCTAssertEqual(header, "application/x-www-form-urlencoded; charset=utf-8")
      
    } catch {
      XCTFail("Could not encode request")
    }
  }
}
