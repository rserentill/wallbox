//
//  JSONParameterEncoderTests.swift
//  
//
//  Created by Roger Serentill Gené on 29/8/22.
//

import XCTest

@testable import NetworkModule

final class JSONParameterEncoderTests: XCTestCase {
  func test_encode_dictionary() {
    let sut = JSONParameterEncoder.self
    
    var request = URLRequest(url: .init(string: "https://api.xerinola.io")!)
    
    do {
      try sut.encode(request: &request, with: ["foo": "bar"])
      
      guard let body = request.httpBody else {
        XCTFail("Request body is empty")
        return
      }
      
      XCTAssertEqual(String(bytes: body, encoding: .utf8), "{\"foo\":\"bar\"}")
      guard let header = request.value(forHTTPHeaderField: "Content-Type") else {
        XCTFail("Content-Type header not added to the request")
        return
      }
      XCTAssertEqual(header, "application/json")
      
    } catch {
      XCTFail("Could not encode request")
    }
  }
  
  func test_encode_object() {
    let sut = JSONParameterEncoder.self
    
    var request = URLRequest(url: .init(string: "https://api.xerinola.io")!)
    
    struct Foo: Encodable {
      let bar: Int
    }
    
    do {
      try sut.encode(request: &request, with: Foo(bar: 123))
      
      guard let body = request.httpBody else {
        XCTFail("Request body is empty")
        return
      }
      
      XCTAssertEqual(String(bytes: body, encoding: .utf8), "{\"bar\":123}")
      guard let header = request.value(forHTTPHeaderField: "Content-Type") else {
        XCTFail("Content-Type header not added to the request")
        return
      }
      XCTAssertEqual(header, "application/json")
    } catch {
      XCTFail("Could not encode request")
    }
  }
}
