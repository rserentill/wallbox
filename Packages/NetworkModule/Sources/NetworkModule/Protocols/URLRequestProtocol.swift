//
//  URLRequestProtocol.swift
//  
//
//  Created by Roger Serentill Gené on 18/8/22.
//

import Foundation

protocol URLRequestProtocol {
  var url: URL? { get }
}

extension URLRequest: URLRequestProtocol {}
