//
//  HTTPURLResponseProtocol.swift
//  
//
//  Created by Roger Serentill Gené on 18/8/22.
//

import Foundation

protocol HTTPURLResponseProtocol {
  var statusCode: Int { get }
}

extension HTTPURLResponse: HTTPURLResponseProtocol {}
