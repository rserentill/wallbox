//
//  String+MD5.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 27/8/21.
//

import Foundation
import CryptoKit

extension String {
  var md5: Self {
    let digest = Insecure.MD5.hash(data: data(using: .utf8) ?? Data())
    return digest.map { Self(format: "%02hhx", $0) }.joined()
  }
}
