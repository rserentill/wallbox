//
//  Token.swift
//  
//
//  Created by Roger Serentill Gené on 7/3/22.
//

import Foundation

public struct Token {
  public let token: String
  public let expirationDate: Date
  
  var status: Status {
    let now = Date()
    
    if now >= expirationDate { return .expired }
    else if now >= expirationDate.addingTimeInterval(-120) { return .aboutToExpire }
    else { return .valid }
  }
  
  enum Status {
    case valid, aboutToExpire, expired
  }
  
  public init(token: String, expirationDate: Date) {
    self.token = token
    self.expirationDate = expirationDate
  }
}
