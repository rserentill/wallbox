//
//  OAuthPlugin.swift
//  
//
//  Created by Roger Serentill Gené on 6/3/22.
//

import Foundation

public protocol OAuthManagerProtocol {
  static var token: Token? { get }
  static func requestToken() async -> Token?
}

public struct OAuthPlugin: Plugin {
  private let manager: OAuthManagerProtocol.Type
  
  func intercept(request: URLRequest) async -> URLRequest {
    var request = request
    
    guard let token = manager.token else { return request }
    
    switch token.status {
      case .valid:
        request.setValue("Bearer \(token.token)", forHTTPHeaderField: "authorization")
        
      case .aboutToExpire, .expired:
        guard let newToken = await manager.requestToken() else { break }
        request.setValue("Bearer \(newToken.token)", forHTTPHeaderField: "authorization")
    }
    
    return request
  }
  
  init(manager: OAuthManagerProtocol.Type) {
    self.manager = manager
  }
}
