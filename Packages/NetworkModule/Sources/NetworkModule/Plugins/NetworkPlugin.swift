//
//  NetworkPlugin.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 27/8/21.
//

public enum NetworkPlugin {
  case
  cURL,
  oAuth(OAuthManagerProtocol.Type),
  logout(LogoutManagerProtocol.Type)
  
  var plugin: Plugin {
    switch self {
      case .cURL:
        return CURLPlugin()
        
      case .oAuth(let manager):
        return OAuthPlugin(manager: manager)
        
      case .logout(let manager):
        return LogoutPlugin(manager: manager)
    }
  }
}
