//
//  Logout.swift
//  
//
//  Created by Roger Serentill Gené on 13/3/22.
//

import Foundation

public protocol LogoutManagerProtocol {
  static var logoutStatusCode: Int { get }
  static func logout()
}

public struct LogoutPlugin: Plugin {
  private let manager: LogoutManagerProtocol.Type
  
  func didReceive(response: HTTPURLResponseProtocol, data: Data?, from request: URLRequestProtocol) {
    guard response.statusCode == manager.logoutStatusCode else { return }
    manager.logout()
  }
  
  init(manager: LogoutManagerProtocol.Type) {
    self.manager = manager
  }
}
