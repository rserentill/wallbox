//
//  CURLPlugin.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 27/8/21.
//

import Foundation

public struct CURLPlugin: Plugin {
  func willPerform(request: URLRequest) -> URLRequest {
    guard let url = request.url else { return request }
    var baseCommand = #"curl "\#(url.absoluteString)""#
    
    if request.httpMethod == "HEAD" {
      baseCommand += " --head"
    }
    
    var command = [baseCommand]
    
    if let method = request.httpMethod, !["GET", "HEAD"].contains(method) {
      command.append("-X \(method)")
    }
    
    if let headers = request.allHTTPHeaderFields {
      for (key, value) in headers where key != "Cookie" {
        command.append("-H '\(key): \(value)'")
      }
    }
    
    if let data = request.httpBody, let body = String(data: data, encoding: .utf8) {
      command.append("-d '\(body)'")
    }
    
    print(command.joined(separator: " \\\n\t"))
    
    return request
  }
  
  func didReceive(response: HTTPURLResponse, data: Data?, from request: URLRequest) {
    guard let method = request.httpMethod, let url = request.url else { return }
    var result = "\n"
    result += "\(method) \(url.relativeString)"
    result += "\n"
    
    switch response.statusCode {
      case 200...299: result += "🟢 "
      case 300...399: result += "🟡 "
      case 400...499: result += "🟠 "
      case 500...599: result += "🔴 "
      
      default: break
    }
    
    result += "\(response.statusCode)"
    
    if
      let data = data,
      let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
      let message = json["message"] as? String {
      result += " \(message)"
    }
    
    result += "\n"
    
    print(result)
  }
}
