//
//  Plugin.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 27/8/21.
//

import Foundation

protocol Plugin {
  func willPerform(request: URLRequest) -> URLRequest
  func intercept(request: URLRequest) async -> URLRequest
  
  func didReceive(response: HTTPURLResponseProtocol, data: Data?, from request: URLRequestProtocol)
}

extension Plugin {
  func willPerform(request: URLRequest) -> URLRequest {
    request
  }
  
  func intercept(request: URLRequest) async -> URLRequest {
    request
  }
  
  func didReceive(response: HTTPURLResponseProtocol, data: Data?, from request: URLRequestProtocol) {}
}
