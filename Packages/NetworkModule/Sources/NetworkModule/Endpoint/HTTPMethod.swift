//
//  HTTPMethod.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 26/8/21.
//

public enum HTTPMethod: String {
  case
  get = "GET",
  post = "POST",
  put = "PUT",
  patch = "PATCH",
  delete = "DELETE"
}
