//
//  Endpoint.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 26/8/21.
//

import Foundation

public enum RequestParameters {
  case
    url(parameters: [String: Any]),
    body(parameters: [String: Any]),
    model(_ object: Encodable)
}

public protocol Endpoint {
  var baseUrl: URL? { get }
  var path: String { get }
  var parameters: RequestParameters? { get }
  var headers: [String: String]? { get }
  var httpMethod: HTTPMethod { get }
}

public extension Endpoint {
  var parameters: RequestParameters? { nil }
  var headers: [String: String]? { nil }
}
