//
//  URLParameterEncoder.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 26/8/21.
//

import Foundation

public struct URLParameterEncoder: ParameterEncodable {
  public static func encode(request: inout URLRequest, with parameters: [String : Any]) throws {
    guard let url = request.url else { throw NetworkError.invalidURL(request.url?.absoluteString) }
    
    request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
    
    guard
      !parameters.isEmpty,
      var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
    else { return }
    
    components.queryItems = parameters.map {
      .init(
        name: $0.key,
        value: "\($0.value)"
      )
    }
    
    request.url = components.url
  }
}
