//
//  AnyEncodable.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 26/8/21.
//

public struct AnyEncodable: Encodable {
  private let encodable: Encodable
  
  public init(_ encodable: Encodable) {
    self.encodable = encodable
  }
  
  public func encode(to encoder: Encoder) throws {
    try encodable.encode(to: encoder)
  }
}
