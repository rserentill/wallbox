//
//  ParameterEncodable.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 26/8/21.
//

import Foundation

public protocol ParameterEncodable {
  static func encode(request: inout URLRequest, with parameters: [String: Any]) throws
}
