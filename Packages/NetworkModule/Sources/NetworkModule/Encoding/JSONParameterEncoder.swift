//
//  JSONParameterEncoder.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 26/8/21.
//

import Foundation

struct JSONParameterEncoder: ParameterEncodable {
  static func encode(request: inout URLRequest, with parameters: [String : Any]) throws {
    do {
      let data = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
      request.httpBody = data
      request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    } catch {
      throw NetworkError.encodingFailed(error)
    }
  }
  
  static func encode(request: inout URLRequest, with object: Encodable) throws {
    let encoder = JSONEncoder()
    do {
      let encodable = AnyEncodable(object)
      request.httpBody = try encoder.encode(encodable)
      request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    } catch {
      throw NetworkError.encodingFailed(error)    }
  }
}
