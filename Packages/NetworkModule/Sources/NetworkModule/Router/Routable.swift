//
//  Routable.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 26/8/21.
//

import Foundation

public protocol Routable: AnyObject {
  associatedtype Endpoint
  
  @discardableResult
  func request<Model: Decodable>(_ endpoint: Endpoint) async throws -> Result<Model, NetworkError>
}
