//
//  Router.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 26/8/21.
//

import Foundation

open class Router<T: Endpoint>: Routable {
  private let plugins: [NetworkPlugin]
  
  public init(plugins: [NetworkPlugin] = []) {
    self.plugins = plugins
  }
  
  @discardableResult
  public func request<Model: Decodable>(_ endpoint: T) async -> Result<Model, NetworkError> {
    do {
      let result = try await performDataTask(endpoint)
      
      switch result {
        case .success(let data):
          do {
            guard let data = data else { return .failure(.invalidResponse) }
            
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .formatted(.iso8601Full)
            
            let model = try decoder.decode(Model.self, from: data)
            return .success(model)
            
          } catch {
            print("❌ Decoding error for \(Model.self):", error)
            return .failure(.encodingFailed(error))
          }
          
        case .failure(let error):
          return .failure(error)
      }
    } catch let error {
      return .failure(.underlying(error))
    }
  }
}

private extension Router {
  func performDataTask(_ endpoint: T) async throws -> Result<Data?, NetworkError> {
    let request = try await build(from: endpoint)
    
    return try await withCheckedThrowingContinuation { continuation in
      
      let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
        if let error = error {
          continuation.resume(throwing: error)
          return
        }
        
        guard let response = response as? HTTPURLResponse else {
          continuation.resume(returning: .failure(.invalidResponse))
          return
        }
        
        self.plugins.forEach {
          $0.plugin.didReceive(response: response, data: data, from: request)
        }
        
        switch response.statusCode {
          case 200...299:
            continuation.resume(returning: .success(data))
          case 401:
            continuation.resume(returning: .failure(.unauthorized))
          case 403:
            continuation.resume(returning: .failure(.forbidden))
          case 400...499:
            continuation.resume(returning: .failure( response.statusCode == 404 ? .notFound : .badRequest ))
          case 500...599:
            continuation.resume(returning: .failure(.internalServer))
          default:
            continuation.resume(returning: .failure(.underlying(nil)))
        }
      })
      
      task.resume()
    }
  }
  
  func build(from endpoint: Endpoint) async throws -> URLRequest {
    let composedUrl = endpoint.baseUrl?.appendingPathComponent(endpoint.path)
    guard let url = composedUrl else {
      throw NetworkError.invalidURL(composedUrl?.absoluteString)
    }
    var request = URLRequest(
      url: url,
      cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
      timeoutInterval: 10
    )
    
    request.httpMethod = endpoint.httpMethod.rawValue
    
    endpoint.headers?.forEach {
      request.setValue($0.value, forHTTPHeaderField: $0.key)
    }
    
    switch endpoint.parameters {
      case .url(let parameters):
        try URLParameterEncoder.encode(request: &request, with: parameters)
      case .body(let parameters):
        try JSONParameterEncoder.encode(request: &request, with: parameters)
      case .model(let object):
        try JSONParameterEncoder.encode(request: &request, with: object)
      default:
        break
    }
    
    let intercepted = await intercept(request: request, plugins: ArraySlice(plugins))
    
    return plugins.reduce(intercepted) {
      $1.plugin.willPerform(request: $0)
    }
  }
  
  func intercept(request: URLRequest, plugins: ArraySlice<NetworkPlugin>) async -> URLRequest {
    guard let plugin = plugins.first else { return request }
    
    return await intercept(
      request: await plugin.plugin.intercept(request: request),
      plugins: plugins.dropFirst()
    )
  }
}
