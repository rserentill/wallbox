//
//  Cancellable.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 27/8/21.
//

import Foundation

public protocol Cancellable {
  func cancel()
}

extension URLSessionTask: Cancellable {}
