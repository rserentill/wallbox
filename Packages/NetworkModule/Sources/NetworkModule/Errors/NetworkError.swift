//
//  NetworkError.swift
//  NetworkModule
//
//  Created by Roger Serentill Gené on 26/8/21.
//

public enum NetworkError: Error {
  // Request
  case connection // No internet
  case invalidURL(_ url: String?)
  case encodingFailed(_ error: Error)
  
  // Response
  case invalidResponse
  case unauthorized // 401
  case forbidden // 403
  case notFound // 404
  case badRequest // 400
  case internalServer // 5xx
  
  case underlying(_ error: Error?)
}
