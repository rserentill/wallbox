//
//  XCTestCase+.swift
//  WallboxTests
//
//  Created by Roger Serentill Gené on 19/9/22.
//

import XCTest

extension XCTestCase {
  func wait(_ condition: @escaping @autoclosure () -> (Bool), timeout: TimeInterval = 1) {
    let expectation = XCTNSPredicateExpectation(predicate: .init(block: { _, _ in condition() }), object: nil)
    wait(for: [expectation], timeout: timeout)
  }
}
