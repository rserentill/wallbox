//
//  DashboardViewModelTests.swift
//  DashboardViewModelTests
//
//  Created by Roger Serentill Gené on 19/9/22.
//

import XCTest
@testable import Wallbox

final class DashboardViewModelTests: XCTestCase {
  func test_viewDidLoad_populate_initial_data() {
    let sut = DashboardViewModel(interactor: DashboardInteractorMock(), coordinator: DashboardCoordinatorMock())
    
    sut.viewDidLoad()
    
    XCTAssertEqual(sut.data[0].section, .quasarEnergy)
    XCTAssertEqual(sut.data[0].items.count, 2)
    XCTAssertEqual(sut.data[1].section, .liveData)
    XCTAssertEqual(sut.data[1].items.count, 1)
    XCTAssertEqual(sut.data[2].section, .statistics)
    XCTAssertEqual(sut.data[2].items.count, 1)
  }
  
  func test_viewDidLoad_populate_statistics() {
    let samples = getFakeSamples(4)
    let interactor = DashboardInteractorMock(historicalData: samples, liveData: getFakeLiveData())
    let sut = DashboardViewModel(interactor: interactor, coordinator: DashboardCoordinatorMock())
    
    sut.viewDidLoad()
    wait(!sut.statistics.isEmpty, timeout: 2)
    
    guard
      let oldest = samples.first?.timestamp,
      let newest = samples.last?.timestamp
    else {
      XCTFail("Fake samples doesn't contain valid dates")
      return
    }
    let elapsedHours = (newest - oldest) / 3600
    
    XCTAssertNil(sut.error)
    XCTAssertEqual(sut.statistics.count, 1)
    XCTAssertEqual(sut.statistics.first?.totalEnergy, samples.reduce(0) { $0 + $1.buildingPower } / elapsedHours)
    XCTAssertEqual(sut.statistics.first?.solarEnergy, samples.reduce(0) { $0 + $1.solarPower } / elapsedHours)
    XCTAssertEqual(sut.statistics.first?.quasarsEnergy, abs(samples.reduce(0) { $0 + $1.quasarPower }) / elapsedHours)
    XCTAssertEqual(sut.statistics.first?.gridEnergy, samples.reduce(0) { $0 + $1.gridPower } / elapsedHours)
  }
  
  func test_viewDidLoad_populate_quasars() {
    let samples = getFakeSamples(10)
    let interactor = DashboardInteractorMock(historicalData: samples, liveData: getFakeLiveData())
    let sut = DashboardViewModel(interactor: interactor, coordinator: DashboardCoordinatorMock())
    
    sut.viewDidLoad()
    wait(!sut.quasars.isEmpty, timeout: 2)
    
    let charges = samples.filter { $0.quasarPower > 0 }.reduce(0) { $0 + $1.quasarPower}
    let discharges = samples.filter { $0.quasarPower < 0 }.reduce(0) { $0 - $1.quasarPower}
    
    XCTAssertNil(sut.error)
    XCTAssertEqual(sut.quasars.count, 2)
    XCTAssertEqual(sut.quasars.first?.flow, .charge)
    XCTAssertEqual(sut.quasars.first?.amount, charges)
    XCTAssertEqual(sut.quasars.last?.flow, .discharge)
    XCTAssertEqual(sut.quasars.last?.amount, discharges)
  }
  
  func test_viewDidLoad_populate_liveData() {
    let liveData = getFakeLiveData()
    let interactor = DashboardInteractorMock(historicalData: getFakeSamples(1), liveData: liveData)
    let sut = DashboardViewModel(interactor: interactor, coordinator: DashboardCoordinatorMock())
    
    sut.viewDidLoad()
    wait(!sut.quasars.isEmpty, timeout: 2)
    
    XCTAssertNil(sut.error)
    XCTAssertEqual(sut.liveData.count, 1)
    XCTAssertEqual(sut.liveData.first?.demand, liveData.buildingDemand)
    XCTAssertEqual(sut.liveData.first?.solarPower, liveData.solarPower)
    XCTAssertEqual(sut.liveData.first?.quasarsPower, abs(liveData.quasarPower))
    XCTAssertEqual(sut.liveData.first?.gridPower, liveData.gridPower)
  }
  
  func test_viewDidLoad_historicalData_error() {
    let interactor = DashboardInteractorMock(liveData: getFakeLiveData())
    let sut = DashboardViewModel(interactor: interactor, coordinator: DashboardCoordinatorMock())
    
    sut.viewDidLoad()
    wait(sut.error != nil, timeout: 2)
    
    XCTAssertEqual(sut.error?.message, "Something went wrong while retrieving historical data! Please try again.")
    XCTAssertEqual(sut.quasars, [])
    XCTAssertEqual(sut.statistics, [])
  }

  func test_viewDidLoad_liveData_error() {
    let interactor = DashboardInteractorMock(historicalData: getFakeSamples(1))
    let sut = DashboardViewModel(interactor: interactor, coordinator: DashboardCoordinatorMock())
    
    sut.viewDidLoad()
    wait(sut.error != nil, timeout: 2)
    
    XCTAssertEqual(sut.error?.message, "Something went wrong while retrieving live data!")
    XCTAssertEqual(sut.liveData, [])
  }
  
  func test_didSelect() {
    let samples = getFakeSamples(3)
    let interactor = DashboardInteractorMock(historicalData: samples)
    let coordinator = DashboardCoordinatorMock()
    let sut = DashboardViewModel(interactor: interactor, coordinator: coordinator)
    
    sut.viewDidLoad()
    wait(!sut.statistics.isEmpty, timeout: 2)
    
    sut.didSelect(item: 0, at: 2)
    
    XCTAssertEqual(coordinator.samples, samples)
  }
}

// MARK: - Fake Data

private extension DashboardViewModelTests {
  func getFakeSample(with timestamp: Date) -> PowerSample {
    .init(
      buildingPower: .random(in: 0 ... 200),
      gridPower: .random(in: 0 ... 200),
      solarPower: .random(in: 0 ... 200),
      quasarPower: .random(in: -200 ... 200),
      timestamp: timestamp
    )
  }
  
  func getFakeSamples(_ count: Int) -> [PowerSample] {
    var result = [PowerSample]()
    
    let calendar = Calendar.current
    let firstDate = calendar.date(byAdding: .hour, value: -count, to: .now)!
    
    for i in 0 ..< count {
      let date = calendar.date(byAdding: .hour, value: i, to: firstDate)!
      result.append(getFakeSample(with: date))
    }
    
    return result
  }
  
  func getFakeLiveData() -> LiveData {
    .init(
      buildingDemand: .random(in: 0 ... 200),
      gridPower: .random(in: 0 ... 200),
      solarPower: .random(in: 0 ... 200),
      quasarPower: .random(in: -200 ... 200))
  }
}
