//
//  DashboardCoordinatorMock.swift
//  WallboxTests
//
//  Created by Roger Serentill Gené on 20/9/22.
//

import XCTest
@testable import Wallbox

final class DashboardCoordinatorMock: DashboardCoordinatorProtocol {
  private(set) var samples: [PowerSample]?
  
  func showDetail(with samples: [PowerSample]) {
    self.samples = samples
  }
}
