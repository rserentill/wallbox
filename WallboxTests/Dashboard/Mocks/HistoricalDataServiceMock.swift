//
//  HistoricalDataServiceMock.swift
//  WallboxTests
//
//  Created by Roger Serentill Gené on 19/9/22.
//

@testable import Wallbox

final class HistoricalDataServiceMock: HistoricalDataServiceProtocol {
  private let result: Result<[PowerSampleDTO], HistoricalDataServiceError>
  
  func fetchHistoricalData() async -> Result<[PowerSampleDTO], HistoricalDataServiceError> {
    result
  }
  
  init(result: Result<[PowerSampleDTO], HistoricalDataServiceError>) {
    self.result = result
  }
}
