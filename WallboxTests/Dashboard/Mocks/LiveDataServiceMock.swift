//
//  LiveDataServiceMock.swift
//  WallboxTests
//
//  Created by Roger Serentill Gené on 19/9/22.
//

@testable import Wallbox

final class LiveDataServiceMock: LiveDataServiceProtocol {
  private let result: Result<LiveDataDTO, LiveDataServiceError>
  
  func fetchLiveData() async -> Result<LiveDataDTO, LiveDataServiceError> {
    result
  }
  
  init(result: Result<LiveDataDTO, LiveDataServiceError>) {
    self.result = result
  }
}
