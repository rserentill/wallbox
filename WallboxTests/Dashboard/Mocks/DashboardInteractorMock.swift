//
//  DashboardInteractorMock.swift
//  WallboxTests
//
//  Created by Roger Serentill Gené on 19/9/22.
//

@testable import Wallbox

final class DashboardInteractorMock: DashboardInteractorProtocol {
  private let historicalData: [PowerSample]
  private let liveData: LiveData?
  
  func fetchHistoricalData() async -> [PowerSample] {
    historicalData
  }
  
  func fetchLiveData() async -> LiveData? {
    liveData
  }
  
  init(historicalData: [PowerSample] = [], liveData: LiveData? = nil) {
    self.historicalData = historicalData
    self.liveData = liveData
  }
}
