//
//  DashboardInteractorTests.swift
//  WallboxTests
//
//  Created by Roger Serentill Gené on 20/9/22.
//

import XCTest
@testable import Wallbox

final class DashboardInteractorTests: XCTestCase {
  func test_fetchHistoricalData_success() async {
    let dtos = getFakeSampleDTOs(4)
    let historicalDataService = HistoricalDataServiceMock(result: .success(dtos))
    let liveDataService = LiveDataServiceMock(result: .failure(.generic))
    
    let sut = DashboardInteractor(historicalDataService: historicalDataService, liveDataService: liveDataService)
    let samples = await sut.fetchHistoricalData()
    
    XCTAssertEqual(samples, dtos.map(mapPowerSampleDTO))
  }
  
  func test_fetchLiveData_success() async {
    let dto = getFakeLiveDataDTO()
    let historicalDataService = HistoricalDataServiceMock(result: .failure(.generic))
    let liveDataService = LiveDataServiceMock(result: .success(dto))
    
    let sut = DashboardInteractor(historicalDataService: historicalDataService, liveDataService: liveDataService)
    let liveData = await sut.fetchLiveData()
    
    XCTAssertEqual(liveData, mapLiveDataDTO(dto: dto))
  }
  
  func test_fetchHistoricalData_error() async {
    let historicalDataService = HistoricalDataServiceMock(result: .failure(.generic))
    let liveDataService = LiveDataServiceMock(result: .success(getFakeLiveDataDTO()))
    
    let sut = DashboardInteractor(historicalDataService: historicalDataService, liveDataService: liveDataService)
    let samples = await sut.fetchHistoricalData()
    
    XCTAssertTrue(samples.isEmpty)
  }
  
  func test_fetchLiveData_error() async {
    let historicalDataService = HistoricalDataServiceMock(result: .success(getFakeSampleDTOs(4)))
    let liveDataService = LiveDataServiceMock(result: .failure(.generic))
    
    let sut = DashboardInteractor(historicalDataService: historicalDataService, liveDataService: liveDataService)
    let liveData = await sut.fetchLiveData()
    
    XCTAssertNil(liveData)
  }
}

extension PowerSample: Equatable {
  public static func == (lhs: PowerSample, rhs: PowerSample) -> Bool {
    lhs.buildingPower == rhs.buildingPower &&
    lhs.solarPower == rhs.solarPower &&
    lhs.quasarPower == rhs.quasarPower &&
    lhs.gridPower == rhs.gridPower &&
    lhs.timestamp?.timeIntervalSince1970 == rhs.timestamp?.timeIntervalSince1970
  }
}

extension LiveData: Equatable {
  public static func == (lhs: LiveData, rhs: LiveData) -> Bool {
    lhs.buildingDemand == rhs.buildingDemand &&
    lhs.solarPower == rhs.solarPower &&
    lhs.quasarPower == rhs.quasarPower &&
    lhs.gridPower == rhs.gridPower
  }
}

// MARK: - Fake Data

private extension DashboardInteractorTests {
  func getFakeSampleDTO(with timestamp: Date) -> PowerSampleDTO {
    .init(
      buildingPower: .random(in: 0 ... 200),
      gridPower: .random(in: 0 ... 200),
      solarPower: .random(in: 0 ... 200),
      quasarPower: .random(in: -200 ... 200),
      timestamp: ISO8601DateFormatter().string(from: timestamp)
    )
  }
  
  func getFakeSampleDTOs(_ count: Int) -> [PowerSampleDTO] {
    var result = [PowerSampleDTO]()
    
    let calendar = Calendar.current
    let firstDate = calendar.date(byAdding: .hour, value: -count, to: .now)!
    
    for i in 0 ..< count {
      let date = calendar.date(byAdding: .hour, value: i, to: firstDate)!
      result.append(getFakeSampleDTO(with: date))
    }
    
    return result
  }
  
  func getFakeLiveDataDTO() -> LiveDataDTO {
    .init(
      buildingDemand: .random(in: 0 ... 200),
      gridPower: .random(in: 0 ... 200),
      solarPower: .random(in: 0 ... 200),
      quasarPower: .random(in: -200 ... 200))
  }
  
  func mapPowerSampleDTO(dto: PowerSampleDTO) -> PowerSample {
    .init(
      buildingPower: dto.buildingPower,
      gridPower: dto.gridPower,
      solarPower: dto.solarPower,
      quasarPower: dto.quasarPower,
      timestamp: ISO8601DateFormatter().date(from: dto.timestamp)
    )
  }
  
  func mapLiveDataDTO(dto: LiveDataDTO) -> LiveData {
    .init(
      buildingDemand: dto.buildingDemand,
      gridPower: dto.gridPower,
      solarPower: dto.solarPower,
      quasarPower: dto.quasarPower
    )
  }
}
