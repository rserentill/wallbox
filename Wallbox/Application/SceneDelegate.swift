//
//  SceneDelegate.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import UIKit

final class SceneDelegate: UIResponder, UIWindowSceneDelegate {
  private var coordinator: CoordinatorProtocol?
  var window: UIWindow?

  func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
    guard let windowScene = scene as? UIWindowScene else { return }
    
    window = UIWindow(frame: windowScene.coordinateSpace.bounds)
    guard let window = window else { return }
    window.windowScene = windowScene
    
    coordinator = RootCoordinator(window: window)
    coordinator?.start()
  }
}
