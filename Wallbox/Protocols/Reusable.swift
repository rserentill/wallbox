//
//  Reusable.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

public protocol Reusable {
  static var reuseIdentifier: String { get }
}

extension Reusable {
  static var reuseIdentifier: String {
    .init(describing: self)
  }
}
