//
//  Configurable.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

protocol Configurable: Reusable {
  associatedtype Model
  func configure(with model: Model)
}
