//
//  DetailCoordinator.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 20/9/22.
//

import UIKit
import SwiftUI

final class DetailCoordinator: Coordinator {
  private let navigationController: UINavigationController
  private let samples: [PowerSample]
  
  init(navigationController: UINavigationController, samples: [PowerSample]) {
    self.navigationController = navigationController
    self.samples = samples
  }
  
  override func start() {
    navigationController.pushViewController(
      UIHostingController(rootView: DetailView(viewModel: .init(samples: samples))),
      animated: true
    )
  }
}
