//
//  DetailViewModel.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 20/9/22.
//

import Foundation
import Charts

final class DetailViewModel: ObservableObject {
  private let samples: [PowerSample]
  
  var title: String {
    guard
      let oldest = samples.first?.timestamp,
      let newest = samples.last?.timestamp
    else { return "Plotted Data" }
    
    return "\(oldest.short) - \(newest.short)"
  }
  
  var binnedData: [PlottableData] {
    let data = samples
    let dates = data.compactMap(\.timestamp)

    let bins = DateBins(data: dates, desiredCount: 25)
    let groups = Dictionary(grouping: data) { bins.index(for: $0.timestamp!) }
    
    let grouped: [PlottableData] = groups.reduce(.init()) { result, pair in
      let (index, samples) = pair
      
      let range = bins[index]

      let solarSamples = samples.map(\.solarPower)
      let solar = solarSamples.reduce(0, +) / Double(solarSamples.count)
      
      let quasarSamples = samples.map(\.quasarPower)
      let quasar = quasarSamples.reduce(0, +) / Double(quasarSamples.count)
      
      let gridSamples = samples.map(\.gridPower)
      let grid = gridSamples.reduce(0, +) / Double(quasarSamples.count)
      
      return result + [
        .init(index: index, source: "Solar", power: solar, range: range),
        .init(index: index, source: "Quasar", power: quasar, range: range),
        .init(index: index, source: "Grid", power: grid, range: range)
      ]
    }
    
    return grouped.sorted { $0.index > $1.index }
  }
  
  init(samples: [PowerSample]) {
    self.samples = samples
  }
}
