//
//  DetailView.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 21/9/22.
//

import Charts
import SwiftUI

struct DetailView: SwiftUI.View {
  private let viewModel: DetailViewModel
  
  var body: some SwiftUI.View {
    VStack {
      Text(viewModel.title)
        .font(.h2)
        .foregroundColor(.highEmphasis)
        .padding(8)
        .fixedSize(horizontal: false, vertical: true)
      GeometryReader { geometry in
        Chart(viewModel.binnedData, id: \.index) { element in
          LineMark(
            x: .value("Timestamp", element.range),
            y: .value("Power", element.power)
          )
          .interpolationMethod(.catmullRom)
          .symbol(.circle)
          .foregroundStyle(by: .value("Source", element.source))
        }
        .chartForegroundStyleScale([
          "Solar": Color.softOrange,
          "Quasar": Color.softGreen,
          "Grid": Color.softBlue
        ])
        .frame(height: geometry.size.height)
        .padding()
        .chartXAxis {
          AxisMarks(values: .automatic) { _ in
            AxisGridLine()
              .foregroundStyle(Color.highEmphasis.opacity(0.7))
            AxisValueLabel()
              .foregroundStyle(Color.highEmphasis)
          }
        }
        .chartYAxis {
          AxisMarks(values: .automatic) { _ in
            AxisGridLine()
              .foregroundStyle(Color.highEmphasis.opacity(0.7))
            AxisValueLabel()
              .foregroundStyle(Color.highEmphasis)
          }
        }
        .chartLegend(position: .top, alignment: .center) {
          HStack {
            HStack {
              Circle()
                .foregroundColor(.softOrange)
                .frame(width: 6, height: 6)
              Text("Solar")
                .font(.paragraph)
                .foregroundColor(.highEmphasis)
            }
            HStack {
              Circle()
                .foregroundColor(.softGreen)
                .frame(width: 6, height: 6)
              Text("Quasar")
                .font(.paragraph)
                .foregroundColor(.highEmphasis)
            }
            HStack {
              Circle()
                .foregroundColor(.softBlue)
                .frame(width: 6, height: 6)
              Text("Grid")
                .font(.paragraph)
                .foregroundColor(.highEmphasis)
            }
          }
        }
      }
      .padding(20)
    }
    .background(Color.background)
  }
  
  init(viewModel: DetailViewModel) {
    self.viewModel = viewModel
  }
}

struct DetailView_Previews: PreviewProvider {
  static var previews: some SwiftUI.View {
    DetailView(viewModel: .init(samples: getFakeSamples(1125)))
  }
}

private extension DetailView_Previews {
  static func getFakeSample(with timestamp: Date) -> PowerSample {
    .init(
      buildingPower: .random(in: 0 ... 200),
      gridPower: .random(in: 0 ... 200),
      solarPower: .random(in: 0 ... 200),
      quasarPower: .random(in: -200 ... 200),
      timestamp: timestamp
    )
  }
  
  static func getFakeSamples(_ count: Int) -> [PowerSample] {
    var result = [PowerSample]()
    
    let calendar = Calendar.current
    let firstDate = calendar.date(byAdding: .minute, value: -count, to: .now)!
    
    for i in 0 ..< count {
      let date = calendar.date(byAdding: .minute, value: i, to: firstDate)!
      result.append(getFakeSample(with: date))
    }
    
    return result
  }
}
