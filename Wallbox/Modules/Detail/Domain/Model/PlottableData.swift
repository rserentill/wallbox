//
//  PlottableData.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 23/9/22.
//

import Charts
import Foundation

struct PlottableData {
  let index: Int
  let source: String
  let power: Double
  let range: ChartBinRange<Date>
}
