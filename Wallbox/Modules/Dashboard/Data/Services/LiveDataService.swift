//
//  LiveDataService.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

import Foundation
import NetworkModule

final class LiveDataService: LiveDataServiceProtocol {
  private let router = Router<EMSEndpoint>(plugins: [.cURL])

  func fetchLiveData() async -> Result<LiveDataDTO, LiveDataServiceError> {
    let result: Result<LiveDataDTO, NetworkError> = await router.request(.fetchLiveData)
    
    switch result {
      case .success(let dto):
        return .success(dto)
      case .failure:
        return .failure(.generic)
    }
  }
}
