//
//  HistoricalDataService.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

import Foundation
import NetworkModule

final class HistoricalDataService: HistoricalDataServiceProtocol {
  private let router = Router<EMSEndpoint>(plugins: [.cURL])
  
  func fetchHistoricalData() async -> Result<[PowerSampleDTO], HistoricalDataServiceError> {
    let result: Result<[PowerSampleDTO], NetworkError> = await router.request(.fetchHistoricalData)
    
    switch result {
      case .success(let dto):
        return .success(dto)
      case .failure:
        return .failure(.generic)
    }
  }
}
