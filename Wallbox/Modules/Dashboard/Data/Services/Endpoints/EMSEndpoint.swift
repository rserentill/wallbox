//
//  EMSEndpoint.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 23/9/22.
//

import NetworkModule

enum EMSEndpoint {
  case
  fetchHistoricalData,
  fetchLiveData
}

extension EMSEndpoint: Endpoint {
  var path: String {
    switch self {
      case .fetchHistoricalData:
        return "/ems/historical"
      case .fetchLiveData:
        return "/ems/live"
    }
  }
  
  var httpMethod: HTTPMethod {
    switch self {
      case .fetchHistoricalData, .fetchLiveData:
        return .get
    }
  }
}
