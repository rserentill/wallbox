//
//  LiveDataServiceError.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

enum LiveDataServiceError: Error {
  case generic
}
