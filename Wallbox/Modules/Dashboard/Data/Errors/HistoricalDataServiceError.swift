//
//  HistoricalDataServiceError.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

enum HistoricalDataServiceError: Error {
  case generic
}
