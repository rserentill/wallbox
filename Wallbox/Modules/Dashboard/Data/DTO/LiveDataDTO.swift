//
//  LiveDataDTO.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

struct LiveDataDTO: Decodable {
  let buildingDemand: Double
  let gridPower: Double
  let solarPower: Double
  let quasarPower: Double
  
  private enum CodingKeys: String, CodingKey {
    case
    buildingDemand = "building_demand",
    gridPower = "grid_power",
    solarPower = "solar_power",
    quasarPower = "quasars_power"
  }
}
