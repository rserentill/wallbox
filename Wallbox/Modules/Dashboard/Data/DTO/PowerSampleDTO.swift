//
//  PowerSampleDTO.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

struct PowerSampleDTO: Decodable {
  let buildingPower: Double
  let gridPower: Double
  let solarPower: Double
  let quasarPower: Double
  let timestamp: String
  
  private enum CodingKeys: String, CodingKey {
    case
    buildingPower = "building_active_power",
    gridPower = "grid_active_power",
    solarPower = "pv_active_power",
    quasarPower = "quasars_active_power",
    timestamp
  }
}
