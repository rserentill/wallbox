//
//  DashboardCoordinator.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import UIKit

protocol DashboardCoordinatorProtocol {
  func showDetail(with samples: [PowerSample])
}

final class DashboardCoordinator: Coordinator {
  private let navigationController: UINavigationController
  
  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
  }
  
  override func start() {
    navigationController.viewControllers = [DashboardViewController(
      viewModel: .init(
        interactor: DashboardInteractor(
          historicalDataService: HistoricalDataService(),
          liveDataService: LiveDataService()
        ),
        coordinator: self
      )
    )]
  }
}

// MARK: - DashboardCoordinatorProtocol

extension DashboardCoordinator: DashboardCoordinatorProtocol {
  func showDetail(with samples: [PowerSample]) {
    start(child: DetailCoordinator(navigationController: navigationController, samples: samples))
  }
}
