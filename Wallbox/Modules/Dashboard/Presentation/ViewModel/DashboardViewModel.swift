//
//  DashboardViewModel.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import Combine
import Foundation

final class DashboardViewModel {
  private let interactor: DashboardInteractorProtocol
  private let coordinator: DashboardCoordinatorProtocol
  
  @Published private(set) var data = [(section: DashboardSection, items: [AnyHashable])]()
  @Published private(set) var quasars = [QuasarEnergyModel]()
  @Published private(set) var liveData = [LiveDataModel]()
  @Published private(set) var statistics = [StatisticsModel]()
  @Published private(set) var error: DashboardViewModelError?
  
  private var samples = [PowerSample]()

  init(interactor: DashboardInteractorProtocol, coordinator: DashboardCoordinatorProtocol) {
    self.interactor = interactor
    self.coordinator = coordinator
  }
  
  func viewDidLoad() {
    data = [
      (section: .quasarEnergy, items: [LoadingModel(isLoading: true), LoadingModel(isLoading: true)]),
      (section: .liveData, items: [LoadingModel(isLoading: true)]),
      (section: .statistics, items: [LoadingModel(isLoading: true)])
    ]
    
    fetchHistoricalData()
    fetchLiveData()
  }
  
  func didSelect(item: Int, at section: Int) {
    switch DashboardSection.allCases[section] {
      case .statistics:
        guard !samples.isEmpty else { return }
        coordinator.showDetail(with: samples)
      case .quasarEnergy, .liveData:
        break
    }
  }
}

// MARK: - Private Methods

private extension DashboardViewModel {
  func fetchHistoricalData() {
    Task {
      samples = await interactor.fetchHistoricalData()
      
      guard
        let oldest = samples.first,
        let newest = samples.last,
        let oldestTimestamp = oldest.timestamp,
        let newestTimestamp = newest.timestamp
      else {
        error = .init(
          title: "Oopsie",
          message: "Something went wrong while retrieving historical data! Please try again.",
          retryAction: (title: "Retry", closure: fetchHistoricalData)
        )
        return
      }
      
      var demand: Double = 0
      var solar: Double = 0
      var quasar: Double = 0
      var quasarCharge: Double = 0
      var quasarDischarge: Double = 0
      var grid: Double = 0
      
      samples.forEach { sample in
        demand += sample.buildingPower
        solar += sample.solarPower
        grid += sample.gridPower
        
        let quasarPower = sample.quasarPower
        quasar += quasarPower
        
        if quasarPower < 0 { quasarDischarge -= quasarPower }
        else { quasarCharge += quasarPower }
      }
      
      updateQuasarEnergy(quasarCharge: quasarCharge, quasarDischarge: quasarDischarge)
      updateStatistics(
        .init(
          demand: demand,
          solar: solar,
          quasar: quasar,
          grid: grid,
          oldestTimestamp: oldestTimestamp,
          newestTimestamp: newestTimestamp
        )
      )
    }
  }
  
  func fetchLiveData() {
    Task {
      guard let liveData = await interactor.fetchLiveData() else {
        error = .init(
          title: "Oopsie",
          message: "Something went wrong while retrieving live data!",
          retryAction: (title: "Retry", closure: fetchLiveData)
        )
        return
      }
      
      updateLiveData(liveData: liveData)
    }
  }
  
  func updateQuasarEnergy(quasarCharge: Double, quasarDischarge: Double) {
    quasars = [
      QuasarEnergyModel(amount: quasarCharge, flow: .charge),
      QuasarEnergyModel(amount: quasarDischarge, flow: .discharge)
    ]
  }
  
  func updateLiveData(liveData: LiveData) {
    self.liveData = [
      LiveDataModel(
        demand: liveData.buildingDemand,
        solarPower: liveData.solarPower,
        quasarsPower: abs(liveData.quasarPower),
        gridPower: liveData.gridPower
      )
    ]
  }
  
  func updateStatistics(_ value: Statistics) {
    var elapsedHours = (value.newestTimestamp - value.oldestTimestamp) / 3600
    elapsedHours = elapsedHours == 0 ? 1 : elapsedHours
    
    statistics = [
      StatisticsModel(
        from: value.oldestTimestamp, to: value.newestTimestamp,
        totalEnergy: value.demand / elapsedHours,
        solarEnergy: value.solar / elapsedHours,
        quasarsEnergy: abs(value.quasar) / elapsedHours,
        gridEnergy: value.grid / elapsedHours
      )
    ]
  }
  
  struct Statistics {
    let demand: Double
    let solar: Double
    let quasar: Double
    let grid: Double
    
    let oldestTimestamp: Date
    let newestTimestamp: Date
  }
}
