//
//  DashboardCollection.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 23/9/22.
//

import UIKit

final class DashboardCollection {
  static let headerKind = "DashboardSectionHeader"
  
  // MARK: Layout
  
  static var layout: UICollectionViewCompositionalLayout {
    let item = NSCollectionLayoutItem(
      layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
    )
    
    let pairItem = NSCollectionLayoutItem(
      layoutSize: .init(widthDimension: .fractionalWidth(1/2), heightDimension: .fractionalHeight(1))
    )
    
    pairItem.contentInsets = .init(top: 0, leading: 10, bottom: 0, trailing: 10)
    
    let fullGroup = NSCollectionLayoutGroup.horizontal(
      layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(1/2.5)),
      subitems: [item]
    )
    
    let pairGroup = NSCollectionLayoutGroup.horizontal(
      layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(1/2.5)),
      subitems: [pairItem, pairItem]
    )
    
    fullGroup.contentInsets = .init(top: 0, leading: 20, bottom: 0, trailing: 20)
    pairGroup.contentInsets = .init(top: 0, leading: 10, bottom: 0, trailing: 10)
    
    return .init { index, _ in
      let layoutSection: NSCollectionLayoutSection
      let section = DashboardSection.allCases[index]
      
      switch section {
        case .quasarEnergy:
          layoutSection = .init(group: pairGroup)
          
        case .liveData, .statistics:
          layoutSection = .init(group: fullGroup)
      }
      
      let header = NSCollectionLayoutBoundarySupplementaryItem(
        layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .estimated(50)),
        elementKind: headerKind,
        alignment: .top
      )
      
      header.contentInsets = .init(top: 0, leading: 20, bottom: 0, trailing: 20)

      layoutSection.boundarySupplementaryItems = [header]
      layoutSection.contentInsets = .init(top: 20, leading: 0, bottom: 30, trailing: 0)
      layoutSection.orthogonalScrollingBehavior = .groupPaging
      return layoutSection
    }
  }
  
  // MARK: Data Source
  
  static func getDataSource(for collectionView: UICollectionView) -> UICollectionViewDiffableDataSource<DashboardSection, AnyHashable> {
    let dataSource = UICollectionViewDiffableDataSource<DashboardSection, AnyHashable>(collectionView: collectionView) { collectionView, indexPath, item in
      switch item {
        case let item as QuasarEnergyModel:
          let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: QuasarEnergyCell.reuseIdentifier,
            for: indexPath
          ) as! QuasarEnergyCell
          
          cell.configure(with: item)
          return cell
          
        case let item as LiveDataModel:
          let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: LiveDataCell.reuseIdentifier,
            for: indexPath
          ) as! LiveDataCell
          
          cell.configure(with: item)
          return cell
          
        case let item as StatisticsModel:
          let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: StatisticsCell.reuseIdentifier,
            for: indexPath
          ) as! StatisticsCell
          
          cell.configure(with: item)
          return cell
          
        case let item as LoadingModel:
          let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: LoadingCell.reuseIdentifier,
            for: indexPath
          ) as! LoadingCell
          
          cell.configure(with: item)
          return cell
          
        default:
          return UICollectionViewCell()
      }
    }
    
    dataSource.supplementaryViewProvider = { (collectionView, kind, indexPath) -> UICollectionReusableView? in
      let header = collectionView.dequeueReusableSupplementaryView(
        ofKind: kind,
        withReuseIdentifier: DashboardSectionHeaderView.reuseIdentifier,
        for: indexPath
      ) as! DashboardSectionHeaderView
      
      header.configure(with: DashboardSection.allCases[indexPath.section])
      return header
    }
    
    return dataSource
  }
  
  // MARK: Register Reusable Views
  
  static func registerReusableViews(for collectionView: UICollectionView) {
    collectionView.register(
      DashboardSectionHeaderView.self,
      forSupplementaryViewOfKind: DashboardCollection.headerKind,
      withReuseIdentifier: DashboardSectionHeaderView.reuseIdentifier
    )
    
    collectionView.register(
      QuasarEnergyCell.self,
      forCellWithReuseIdentifier: QuasarEnergyCell.reuseIdentifier
    )
    
    collectionView.register(
      LiveDataCell.self,
      forCellWithReuseIdentifier: LiveDataCell.reuseIdentifier
    )
    
    collectionView.register(
      StatisticsCell.self,
      forCellWithReuseIdentifier: StatisticsCell.reuseIdentifier
    )
    
    collectionView.register(
      LoadingCell.self,
      forCellWithReuseIdentifier: LoadingCell.reuseIdentifier
    )
  }
}
