//
//  DashboardSectionHeaderView.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import UIKit

final class DashboardSectionHeaderView: UICollectionReusableView {
  private lazy var titleLabel = UILabel()
  
  override init(frame: CGRect) {
    super.init(frame: .zero)
    setupTitleLabel()
  }
  
  // MARK: Required init
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - Configurable

extension DashboardSectionHeaderView: Configurable {
  func configure(with model: DashboardSection) {
    titleLabel.text = model.rawValue
  }
}

// MARK: - Layout

private extension DashboardSectionHeaderView {
  func setupTitleLabel() {
    addSubview(titleLabel) { make in
      make.edges.equalToSuperview()
    }
    
    titleLabel.font = .h1
    titleLabel.textColor = .highEmphasis
  }
}
