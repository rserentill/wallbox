//
//  LiveDataCell.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

import Combine
import UIKit

final class LiveDataCell: DashboardCell {
  private lazy var titleLabel = UILabel()
  private lazy var demandLabel = UILabel()
  
  private lazy var solarSlider = SliderView(style: .init(color: .softOrange))
  private lazy var quasarSlider = SliderView(style: .init(color: .softGreen))
  private lazy var gridSlider = SliderView(style: .init(color: .softBlue))
  
  private lazy var stackView = UIStackView(arrangedSubviews: [
    solarSlider,
    quasarSlider,
    gridSlider
  ])

  override func setup() {
    setupTitleLabel()
    setupDemandLabel()
    setupStackView()
  }
}

// MARK: - Configurable

extension LiveDataCell: Configurable {
  func configure(with model: LiveDataModel) {
    let (solarPowerText, solarFraction) = calculatePower(from: model.solarPower, total: model.demand)
    let (quasarPowerText, quasarFraction) = calculatePower(from: model.quasarsPower, total: model.demand)
    let (gridPowerText, gridFraction) = calculatePower(from: model.gridPower, total: model.demand)
    
    demandLabel.text = "\(String(format: "%.2f", model.demand)) kWh"
    
    solarSlider.setup(label: "Solar - \(solarPowerText)", fraction: solarFraction)
    quasarSlider.setup(label: "Quasars - \(quasarPowerText)", fraction: quasarFraction)
    gridSlider.setup(label: "Grid - \(gridPowerText)", fraction: gridFraction)
  }
}

// MARK: - Layout

private extension LiveDataCell {
  func setupTitleLabel() {
    contentView.addSubview(titleLabel) { make in
      make.top.equalToSuperview().inset(18)
      make.leading.equalToSuperview().inset(20)
    }
    
    titleLabel.setContentHuggingPriority(.required, for: .horizontal)
    
    titleLabel.text = "Building demand:"
    
    titleLabel.font = .h4
    titleLabel.textColor = .highEmphasis
  }
  
  func setupDemandLabel() {
    contentView.addSubview(demandLabel) { make in
      make.top.equalTo(titleLabel)
      make.leading.equalTo(titleLabel.snp.trailing).offset(2)
      make.trailing.equalToSuperview().inset(20)
    }
    
    demandLabel.font = .h3
    demandLabel.textColor = .highEmphasis
  }
  
  func setupStackView() {
    contentView.addSubview(stackView) { make in
      make.leading.trailing.equalToSuperview().inset(20)
      make.centerY.equalToSuperview().offset(12)
    }
    
    stackView.axis = .vertical
    stackView.distribution = .equalSpacing
    stackView.spacing = 8
  }
}

// MARK: - Private Methods

private extension LiveDataCell {
  func calculatePower(from value: Double, total: Double) -> (String, Double) {
    let fraction = value/total
    return ("\(String(format: "%.2f", value)) kW (\(String(format: "%.2f", fraction * 100))%)", fraction)
  }
}
