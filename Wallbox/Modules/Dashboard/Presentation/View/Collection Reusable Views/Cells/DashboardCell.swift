//
//  DashboardCell.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import UIKit

class DashboardCell: UICollectionViewCell {
  private lazy var floatingBox = FloatingBox()
  
  override var contentView: UIView {
    floatingBox
  }
  
  func setup() {
    // To be overriden
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    setupFloatingBox()
    setup()
    
  }
  
  // MARK: Required init
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - Layout

private extension DashboardCell {
  func setupFloatingBox() {
    super.contentView.addSubview(floatingBox) { make in
      make.edges.equalToSuperview()
    }
  }
}
