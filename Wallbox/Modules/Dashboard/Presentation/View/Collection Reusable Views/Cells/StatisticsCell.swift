//
//  StatisticsCell.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

import Combine
import UIKit

final class StatisticsCell: DashboardCell {
  private lazy var titleLabel = UILabel()
  
  private lazy var donut = DonutChart()
  
  private lazy var solarLegend = LegendView(color: .softOrange)
  private lazy var quasarLegend = LegendView(color: .softGreen)
  private lazy var gridLegend = LegendView(color: .softBlue)
  
  private lazy var stackView = UIStackView(arrangedSubviews: [
    solarLegend,
    quasarLegend,
    gridLegend
  ])

  override func setup() {
    setupTitleLabel()
    setupDonut()
    setupStackView()
  }
}

// MARK: - Configurable

extension StatisticsCell: Configurable {
  func configure(with model: StatisticsModel) {
    titleLabel.text = "\(model.from.short) - \(model.to.short)"
    
    let (solarPowerText, solarFraction) = calculatePower(from: model.solarEnergy, total: model.totalEnergy)
    let (quasarPowerText, quasarFraction) = calculatePower(from: model.quasarsEnergy, total: model.totalEnergy)
    let (gridPowerText, gridFraction) = calculatePower(from: model.gridEnergy, total: model.totalEnergy)
    
    donut.setup(arcs: [
      .init(value: solarFraction, color: .softOrange),
      .init(value: quasarFraction, color: .softGreen),
      .init(value: gridFraction, color: .softBlue)
    ])
    
    solarLegend.setup(label: "Solar - \(solarPowerText)")
    quasarLegend.setup(label: "Quasars - \(quasarPowerText)")
    gridLegend.setup(label: "Grid - \(gridPowerText)")
  }
}

// MARK: - Layout

private extension StatisticsCell {
  func setupTitleLabel() {
    contentView.addSubview(titleLabel) { make in
      make.top.equalToSuperview().inset(18)
      make.leading.trailing.equalToSuperview().inset(20)
    }
    
    titleLabel.font = .h3
    titleLabel.textColor = .highEmphasis
    titleLabel.textAlignment = .center
  }
  
  func setupDonut() {
    contentView.addSubview(donut) { make in
      make.width.equalTo(donut.snp.height)
      make.top.equalTo(titleLabel.snp.bottom).offset(12)
      make.bottom.equalToSuperview().inset(12)
      make.centerX.equalToSuperview().multipliedBy(0.5)
    }
  }
  
  func setupStackView() {
    contentView.addSubview(stackView) { make in
      make.centerX.equalToSuperview().multipliedBy(1.5)
      make.centerY.equalTo(donut)
    }
    
    stackView.axis = .vertical
    stackView.distribution = .equalSpacing
    stackView.spacing = 8
  }
}

// MARK: - Private Methods

private extension StatisticsCell {
  func calculatePower(from value: Double, total: Double) -> (String, CGFloat) {
    let fraction = CGFloat(value/total)
    return ("\(String(format: "%.2f", fraction * 100))%", fraction)
  }
}
