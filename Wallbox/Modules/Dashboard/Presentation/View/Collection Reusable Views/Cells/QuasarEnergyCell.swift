//
//  QuasarEnergyCell.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import UIKit


final class QuasarEnergyCell: DashboardCell {
  private lazy var amountLabel = UILabel()
  private lazy var amountUnitsLabel = UILabel()
  private lazy var titleLabel = UILabel()
  
  override func setup() {
    setupAmountLabel()
    setupAmountUnitsLabel()
    setupTitleLabel()
  }
}

// MARK: - Configurable

extension QuasarEnergyCell: Configurable {
  func configure(with model: QuasarEnergyModel) {
    amountLabel.text = .init(format: "%.1f", model.amount)
    amountUnitsLabel.text = "kWh"
    
    switch model.flow {
      case .charge:
        titleLabel.text = "Charged"
        amountLabel.textColor = .softBlue
        
      case .discharge:
        titleLabel.text = "Discharged"
        amountLabel.textColor = .softOrange
    }
  }
}

// MARK: - Layout

private extension QuasarEnergyCell {
  func setupAmountLabel() {
    contentView.addSubview(amountLabel) { make in
      make.bottom.equalTo(contentView.snp.centerY)
      make.leading.trailing.equalToSuperview().inset(16)
    }
    
    amountLabel.font = .emphasis
    amountLabel.textAlignment = .center
  }
  
  func setupAmountUnitsLabel() {
    contentView.addSubview(amountUnitsLabel) { make in
      make.top.equalTo(contentView.snp.centerY)
      make.leading.trailing.equalToSuperview().inset(16)
    }
    
    amountUnitsLabel.font = .h4
    amountUnitsLabel.textColor = .darkBlue
    amountUnitsLabel.textAlignment = .center
  }
  
  func setupTitleLabel() {
    contentView.addSubview(titleLabel) { make in
      make.leading.trailing.bottom.equalToSuperview().inset(8)
    }
    
    titleLabel.font = .h6
    titleLabel.textColor = .highEmphasis
    titleLabel.textAlignment = .center
  }
}
