//
//  LoadingCell.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 19/9/22.
//

import UIKit

final class LoadingCell: DashboardCell {
  private lazy var activityIndicator = UIActivityIndicatorView(style: .medium)
  
  override func setup() {
    setupActivityIndicator()
  }
}

// MARK: - Configurable

extension LoadingCell: Configurable {
  func configure(with model: LoadingModel) {
    model.isLoading ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
  }
}

// MARK: - Layout

private extension LoadingCell {
  func setupActivityIndicator() {
    contentView.addSubview(activityIndicator) { make in
      make.center.equalToSuperview()
    }
    
    activityIndicator.color = .highEmphasis
    activityIndicator.startAnimating()
    activityIndicator.hidesWhenStopped = true
  }
}
