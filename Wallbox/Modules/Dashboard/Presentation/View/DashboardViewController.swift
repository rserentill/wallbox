//
//  DashboardViewController.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import Combine
import UIKit

final class DashboardViewController: ViewController {
  private let viewModel: DashboardViewModel
  
  private lazy var dataSource = DashboardCollection.getDataSource(for: collectionView)
  private lazy var collectionView = UICollectionView(frame: .zero, collectionViewLayout: DashboardCollection.layout)
  
  private var cancellables = Set<AnyCancellable>()
  
  init(viewModel: DashboardViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView.backgroundColor = .background

    bindCollectionData()
    bindErrors()
    
    viewModel.viewDidLoad()
  }
  
  override func setup() {
    setupCollectionView()
  }
  
  // MARK: Required init
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - UICollectionViewDelegate

extension DashboardViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    viewModel.didSelect(item: indexPath.item, at: indexPath.section)
  }
}

// MARK: - Layout

private extension DashboardViewController {
  func setupCollectionView() {
    view.addSubview(collectionView) { make in
      make.edges.equalTo(view.safeAreaLayoutGuide)
    }
    
    collectionView.delegate = self
    DashboardCollection.registerReusableViews(for: collectionView)
  }
}

// MARK: - Private Methods

private extension DashboardViewController {
  func append(sections: [(section: DashboardSection, items: [AnyHashable])], animated: Bool = false) {
    var snapshot = NSDiffableDataSourceSnapshot<DashboardSection, AnyHashable>()
    
    snapshot.appendSections(sections.map(\.section))
    sections.forEach {
      snapshot.appendItems($0.items, toSection: $0.section)
    }
    
    dataSource.apply(snapshot, animatingDifferences: animated)
  }
  
  func insert(items: [AnyHashable], to section: DashboardSection, animated: Bool = true, reset: Bool = true) {
    var snapshot = dataSource.snapshot()
    
    if reset {
      snapshot.deleteItems(snapshot.itemIdentifiers(inSection: section))
    }
    
    snapshot.appendItems(items, toSection: section)
    
    dataSource.apply(snapshot, animatingDifferences: animated)
  }
}

// MARK: - Bindings

private extension DashboardViewController {
  func bindCollectionData() {
    viewModel.$data
      .receive(on: DispatchQueue.main)
      .sink {[weak self] in
        guard !$0.isEmpty else { return }
        self?.append(sections: $0)
      }.store(in: &cancellables)
    
    viewModel.$quasars
      .receive(on: DispatchQueue.main)
      .sink {[weak self] in
        guard !$0.isEmpty else { return }
        self?.insert(items: $0, to: .quasarEnergy)
      }.store(in: &cancellables)
    
    viewModel.$liveData
      .receive(on: DispatchQueue.main)
      .sink {[weak self] in
        guard !$0.isEmpty else { return }
        self?.insert(items: $0, to: .liveData)
      }.store(in: &cancellables)
    
    viewModel.$statistics
      .receive(on: DispatchQueue.main)
      .sink {[weak self] in
        guard !$0.isEmpty else { return }
        self?.insert(items: $0, to: .statistics)
      }.store(in: &cancellables)
  }
  
  func bindErrors() {
    viewModel.$error
      .receive(on: DispatchQueue.main)
      .compactMap { $0 }
      .sink {[weak self] error in
        guard let self else { return }
        
        if let action = error.retryAction {
          self.showError(
            title: error.title, message: error.message,
            actions: [
              .init(title: action.title, style: .default) { _ in
                action.closure()
              }
            ]
          )
        }
        else {
          self.showError(title: error.title, message: error.message)
        }
      }.store(in: &cancellables)
  }
}
