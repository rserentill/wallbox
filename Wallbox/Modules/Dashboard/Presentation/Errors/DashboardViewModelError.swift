//
//  DashboardViewModelError.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 19/9/22.
//

struct DashboardViewModelError {
  let title: String
  let message: String?
  let retryAction: (title: String, closure: () -> Void)?
}
