//
//  LiveDataModel.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

struct LiveDataModel: Hashable {
  let demand: Double
  let solarPower: Double
  let quasarsPower: Double
  let gridPower: Double
}
