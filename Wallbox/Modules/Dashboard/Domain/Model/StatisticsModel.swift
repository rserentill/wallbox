//
//  StatisticsModel.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

import Foundation

struct StatisticsModel: Hashable {
  let from: Date
  let to: Date
  let totalEnergy: Double
  let solarEnergy: Double
  let quasarsEnergy: Double
  let gridEnergy: Double
}
