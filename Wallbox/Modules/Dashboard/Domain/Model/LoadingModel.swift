//
//  LoadingModel.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 19/9/22.
//

import Foundation

struct LoadingModel: Hashable {
  private let uuid = UUID()
  let isLoading: Bool
  
  init(isLoading: Bool) {
    self.isLoading = isLoading
  }
}
