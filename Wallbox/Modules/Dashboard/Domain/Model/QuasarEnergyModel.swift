//
//  QuasarEnergyModel.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

struct QuasarEnergyModel: Hashable {
  let amount: Double
  let flow: EnergyFlow
  
  enum EnergyFlow {
    case charge, discharge
  }
}
