//
//  DashboardSection.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 23/9/22.
//

enum DashboardSection: String, CaseIterable {
  case
  quasarEnergy = "Quasar Chargers",
  liveData = "Live Data",
  statistics = "Statistics"
}
