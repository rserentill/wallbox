//
//  LiveData.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

struct LiveData {
  let buildingDemand: Double
  let gridPower: Double
  let solarPower: Double
  let quasarPower: Double
}
