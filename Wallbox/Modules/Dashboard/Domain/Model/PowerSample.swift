//
//  PowerSample.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

import Foundation

struct PowerSample {
  let buildingPower: Double
  let gridPower: Double
  let solarPower: Double
  let quasarPower: Double
  let timestamp: Date?
}
