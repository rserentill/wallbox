//
//  DashboardInteractor.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//
//

import Foundation

final class DashboardInteractor {
  private let historicalDataService: HistoricalDataServiceProtocol
  private let liveDataService: LiveDataServiceProtocol
  
  init(historicalDataService: HistoricalDataServiceProtocol, liveDataService: LiveDataServiceProtocol) {
    self.historicalDataService = historicalDataService
    self.liveDataService = liveDataService
  }
}

// MARK: - DashboardInteractorProtocol

extension DashboardInteractor: DashboardInteractorProtocol {
  func fetchHistoricalData() async -> [PowerSample] {
    let result = await historicalDataService.fetchHistoricalData()
    
    guard case .success(let dtos) = result else { return [] }
    
    return dtos.map {
      .init(
        buildingPower: $0.buildingPower,
        gridPower: $0.gridPower,
        solarPower: $0.solarPower,
        quasarPower: $0.quasarPower,
        timestamp: getDate(from: $0.timestamp)
      )
    }
  }
  
  func fetchLiveData() async -> LiveData? {
    let result = await liveDataService.fetchLiveData()
    
    guard case .success(let dto) = result else { return nil }
    
    return .init(
      buildingDemand: dto.buildingDemand,
      gridPower: dto.gridPower,
      solarPower: dto.solarPower,
      quasarPower: dto.quasarPower
    )
  }
}

// MARK: - Private Methods

private extension DashboardInteractor {
  func getDate(from string: String) -> Date? {
    ISO8601DateFormatter().date(from: string)
  }
}
