//
//  DashboardInteractorProtocol.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 19/9/22.
//

protocol DashboardInteractorProtocol {
  func fetchHistoricalData() async -> [PowerSample]
  func fetchLiveData() async -> LiveData?
}
