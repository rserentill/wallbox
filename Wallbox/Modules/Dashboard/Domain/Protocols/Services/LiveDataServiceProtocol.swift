//
//  LiveDataServiceProtocol.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

protocol LiveDataServiceProtocol {
  func fetchLiveData() async -> Result<LiveDataDTO, LiveDataServiceError>
}
