//
//  HistoricalDataServiceProtocol.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

protocol HistoricalDataServiceProtocol {
  func fetchHistoricalData() async -> Result<[PowerSampleDTO], HistoricalDataServiceError>
}
