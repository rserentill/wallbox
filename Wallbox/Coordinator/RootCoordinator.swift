//
//  RootCoordinator.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 20/9/22.
//

import UIKit

final class RootCoordinator: Coordinator {
  private let window: UIWindow
  
  init(window: UIWindow) {
    self.window = window
  }
  
  override func start() {
    let navigationController = NavigationController()
    
    window.rootViewController = navigationController
    start(child: DashboardCoordinator(navigationController: navigationController))
    window.makeKeyAndVisible()
  }
}
