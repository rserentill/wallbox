//
//  Coordinator.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import Foundation

protocol CoordinatorProtocol: AnyObject {
  func start()
  func finish()
}

class Coordinator: CoordinatorProtocol {
  private var id: String { .init(describing: self) }
  
  private weak var parent: Coordinator?
  private var children = [Coordinator]()
  
  func start(child coordinator: Coordinator) {
    addChild(coordinator)
    coordinator.start()
  }
  
  func finishChild(with id: String) {
    children.removeAll { $0.id == id }
  }
  
  func start() {
    // To be overriden
  }
  
  func finish() {
    children.removeAll()
    parent?.finishChild(with: id)
  }
}

private extension Coordinator {
  func addChild(_ child: Coordinator) {
    child.parent = self
    children.append(child)
  }
}
