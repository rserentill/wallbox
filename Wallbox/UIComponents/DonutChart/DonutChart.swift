//
//  DonutChart.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

import UIKit

final class DonutChart: View {
  private var arcs = [Arc]()
  private var thickness: CGFloat = 0
  
  func setup(arcs: [Arc], thickness: CGFloat = 24) {
    self.arcs = arcs
    self.thickness = thickness
    
    layoutIfNeeded()
  }
  
  override func setup() {
    backgroundColor = .clear
  }
  
  override func draw(_ rect: CGRect) {
    let total = arcs.map(\.value).reduce(0, +)
    
    var startAngle: CGFloat = -.pi/2
    var endAngle: CGFloat = 0.0
    
    arcs.forEach { arc in
      endAngle = (arc.value / total) * (.pi * 2)
      
      let arcPath = UIBezierPath()
      arcPath.addArc(
        withCenter: .init(x: rect.midX, y: rect.midY),
        radius: frame.width / 2 - thickness / 2,
        startAngle: startAngle,
        endAngle: startAngle + endAngle,
        clockwise: true
      )
      
      arcPath.lineWidth = thickness
      arc.color.setStroke()
      arcPath.stroke()
      
      startAngle += endAngle
    }
  }
}

// MARK: - Data Structures

extension DonutChart {
  struct Arc {
    let value: CGFloat
    let color: UIColor
  }
}
