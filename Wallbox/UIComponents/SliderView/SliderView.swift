//
//  SliderView.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

import UIKit

final class SliderView: View {
  private let style: Style
  
  private lazy var label = UILabel()
  private lazy var sliderWrapper = UIView()
  private lazy var leadingLine = UIView()
  private lazy var trailingLine = UIView()
  private lazy var bubble = UIView()
  
  init(style: Style) {
    self.style = style
    super.init(frame: .zero)
  }
  
  override func setup() {
    setupLabel()
    setupSliderWrapper()
    setupBubble()
    setupLeadingLine()
    setupTrailingLine()
  }
  
  func setup(label: String, fraction: Double) {
    self.label.text = label
    updateBubblePosition(to: fraction)
  }
  
  // MARK: - Required init
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - Layout

private extension SliderView {
  func setupLabel() {
    addSubview(label) { make in
      make.top.bottom.leading.equalToSuperview()
      make.trailing.greaterThanOrEqualTo(snp.centerX).offset(20)
    }
    
    label.font = .paragraph
    label.textColor = .highEmphasis
  }
  
  func setupSliderWrapper() {
    addSubview(sliderWrapper) { make in
      make.top.bottom.equalToSuperview()
      make.trailing.equalToSuperview()
      make.leading.equalTo(label.snp.trailing)
    }
  }
  
  func setupBubble() {
    sliderWrapper.addSubview(bubble) { make in
      make.size.equalTo(8)
      make.centerY.equalToSuperview()
      make.centerX.equalTo(sliderWrapper.snp.leading)
    }
    
    bubble.layer.cornerRadius = 4
    bubble.backgroundColor = style.color
  }
  
  func setupLeadingLine() {
    addSubview(leadingLine) {[self] make in
      make.height.equalTo(style.thickness)
      make.centerY.equalToSuperview()
      make.leading.equalTo(label.snp.trailing)
      make.trailing.equalTo(bubble.snp.centerX)
    }
    
    leadingLine.backgroundColor = style.color
    leadingLine.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    leadingLine.layer.cornerRadius = CGFloat(style.thickness) / 2
  }
  
  func setupTrailingLine() {
    addSubview(trailingLine) {[self] make in
      make.height.equalTo(style.thickness)
      make.centerY.equalToSuperview()
      make.leading.equalTo(bubble.snp.centerX)
      make.trailing.equalToSuperview()
    }
    
    trailingLine.backgroundColor = style.color.withAlphaComponent(0.25)
    trailingLine.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
    trailingLine.layer.cornerRadius = CGFloat(style.thickness) / 2
  }
}

// MARK: - Private Methods

private extension SliderView {
  func updateBubblePosition(to fraction: Double) {
    bubble.snp.updateConstraints { update in
      layoutIfNeeded()
      update.centerX.equalTo(sliderWrapper.snp.leading).offset(sliderWrapper.bounds.width * CGFloat(fraction))
    }
  }
}

// MARK: - Data Structures

extension SliderView {
  struct Style {
    let color: UIColor
    let thickness: Float
    
    init(color: UIColor, thickness: Float = 2) {
      self.color = color
      self.thickness = thickness
    }
  }
}
