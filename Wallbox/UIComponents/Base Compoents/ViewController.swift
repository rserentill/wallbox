//
//  ViewController.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import UIKit

class ViewController: UIViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .background
    navigationItem.backBarButtonItem = .init(title: "", style: .plain, target: nil, action: nil)
    
    setup()
  }
  
  func setup() {
    // To be overriden
  }
  
  func showError(title: String?, message: String?, actions: [UIAlertAction] = [.init(title: "Ok", style: .default)]) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    actions.forEach(alert.addAction)
    present(alert, animated: true)
  }
}
