//
//  View.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import UIKit

class View: UIView {
  private var isFirstAppearance = true
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    
    guard isFirstAppearance else { return }
    
    isFirstAppearance = false
    setup()
  }
  
  func setup() {
    // To be overriden
  }
}
