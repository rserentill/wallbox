//
//  NavigationController.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 20/9/22.
//

import UIKit

final class NavigationController: UINavigationController {
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let icon = UIImage.backArrow
    let appearance = UINavigationBar.appearance(whenContainedInInstancesOf: [Self.self])
    
    appearance.backIndicatorImage = icon
    appearance.backIndicatorTransitionMaskImage = icon
    appearance.tintColor = .highEmphasis
    
    navigationBar.setBackgroundImage(.init(), for: .default)
    navigationBar.shadowImage = .init()
    navigationBar.isTranslucent = true
    
    let barButtonAppearance = UIBarButtonItem.appearance(whenContainedInInstancesOf: [Self.self])
    barButtonAppearance.setBackButtonTitlePositionAdjustment(.init(horizontal: -28, vertical: 0), for: .default)
  }
}
