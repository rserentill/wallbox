//
//  LegendView.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

import UIKit

final class LegendView: View {
  private let color: UIColor
  
  private lazy var box = UIView()
  private lazy var label = UILabel()
  
  init(color: UIColor) {
    self.color = color
    
    super.init(frame: .zero)
  }
  
  func setup(label: String) {
    self.label.text = label
  }
  
  override func setup() {
    setupBox()
    setupLabel()
  }
  
  // MARK: Required init
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - Layout

private extension LegendView {
  func setupBox() {
    addSubview(box) { make in
      make.size.equalTo(14)
      make.leading.top.bottom.equalToSuperview()
    }
    
    box.layer.cornerRadius = 4
    box.backgroundColor = color
  }
  
  func setupLabel() {
    addSubview(label) { make in
      make.top.bottom.trailing.equalToSuperview()
      make.leading.equalTo(box.snp.trailing).offset(8)
    }
    
    label.font = .paragraph
    label.textColor = .highEmphasis
  }
}
