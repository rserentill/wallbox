//
//  FloatingBox.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import UIKit

final class FloatingBox: View {
  private lazy var contentView = UIView()
  
  override func addSubview(_ view: UIView) {
    contentView.addSubview(view)
  }
  
  override func setup() {
    setupContentView()
  }
}

// MARK: - Layout

private extension FloatingBox {
  func setupContentView() {
    super.addSubview(contentView)
    
    contentView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
    
    contentView.backgroundColor = .elevatedBackground
    
    layer.cornerRadius = 12
    contentView.layer.cornerRadius = 12
    contentView.clipsToBounds = true
    
    layer.shadowColor = UIColor.black.cgColor
    layer.shadowOpacity = 0.04
    layer.shadowOffset = .init(width: 0, height: 2)
    layer.shadowRadius = 6
  }
}
