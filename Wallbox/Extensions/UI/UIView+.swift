//
//  UIView+.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import UIKit
import SnapKit

public extension UIView {
  func addSubview<T: UIView>(_ value: T = .init()) -> T {
    addSubview(value)
    return value
  }
  
  @discardableResult
  func addSubview<T: UIView>(_ value: T = .init(), constraintsClosure: (ConstraintMaker) -> Void) -> T {
    addSubview(value)
    value.snp.makeConstraints(constraintsClosure)
    return value
  }
  
  @discardableResult
  func addSubview<T: UIView>(_ value: T = .init(), constraintsClosure: (ConstraintMaker, ConstraintViewDSL) -> Void) -> T {
    addSubview(value) {
      constraintsClosure($0, value.snp)
    }
  }
}
