//
//  UIFont+.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import UIKit

extension UIFont {
  static var h1: UIFont { .bold(32) }
  static var h2: UIFont { .bold(20) }
  static var h3: UIFont { .medium(14) }
  static var h4: UIFont { .regular(14) }
  static var h5: UIFont { .semibold(12) }
  static var h6: UIFont { .medium(12) }

  static var emphasis: UIFont { .semibold(40) }
  static var paragraph: UIFont { .regular(12) }
  static var tag: UIFont { .regular(10) }

  static func regular(_ size: CGFloat) -> UIFont {
    UIFont(name: "Montserrat-Regular", size: size)!
  }

  static func medium(_ size: CGFloat) -> UIFont {
    UIFont(name: "Montserrat-Medium", size: size)!
  }

  static func bold(_ size: CGFloat, italic: Bool = false) -> UIFont {
    UIFont(name: "Montserrat-Bold", size: size)!
  }
  
  static func semibold(_ size: CGFloat, italic: Bool = false) -> UIFont {
    UIFont(name: "Montserrat-SemiBold", size: size)!
  }
}
