//
//  UIColor+.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 17/9/22.
//

import UIKit

extension UIColor {
  static let background = UIColor(named: "Background")!
  static let elevatedBackground = UIColor(named: "Elevated Background")!
  
  static let highEmphasis = UIColor(named: "High Emphasis")!

  static let darkBlue = UIColor(named: "Dark Blue")!
  
  static let softBlue = UIColor(named: "Soft Blue")!
  static let softGreen = UIColor(named: "Soft Green")!
  static let softOrange = UIColor(named: "Soft Orange")!
}
