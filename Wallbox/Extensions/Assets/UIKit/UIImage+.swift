//
//  UIImage+.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 20/9/22.
//

import UIKit

extension UIImage {
  static let backArrow = UIImage(named: "Back Arrow")!
}
