//
//  Color+.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 22/9/22.
//

import SwiftUI

extension Color {
  static let background = Color(uiColor: .background)
  static let elevatedBackground = Color(uiColor: .elevatedBackground)
  
  static let highEmphasis = Color(uiColor: .highEmphasis)

  static let darkBlue = Color(uiColor: .darkBlue)
  
  static let softBlue = Color(uiColor: .softBlue)
  static let softGreen = Color(uiColor: .softGreen)
  static let softOrange = Color(uiColor: .softOrange)
}
