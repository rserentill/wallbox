//
//  Font+.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 22/9/22.
//

import SwiftUI

extension Font {
  static var h1: Font { .bold(32) }
  static var h2: Font { .bold(20) }
  static var h3: Font { .medium(14) }
  static var h4: Font { .regular(14) }
  static var h5: Font { .semibold(12) }
  static var h6: Font { .medium(12) }

  static var emphasis: Font { .semibold(40) }
  static var paragraph: Font { .regular(12) }
  static var tag: Font { .regular(10) }
  
  static func regular(_ size: CGFloat) -> Font {
    .custom("Montserrat-Regular", size: size)
  }

  static func medium(_ size: CGFloat) -> Font {
    .custom("Montserrat-Medium", size: size)
  }

  static func bold(_ size: CGFloat, italic: Bool = false) -> Font {
    .custom("Montserrat-Bold", size: size)
  }
  
  static func semibold(_ size: CGFloat, italic: Bool = false) -> Font {
    .custom("Montserrat-SemiBold", size: size)
  }
}
