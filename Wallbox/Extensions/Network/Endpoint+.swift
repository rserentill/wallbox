//
//  Endpoint+.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 23/9/22.
//

import Foundation
import NetworkModule

extension Endpoint {
  var baseUrl: URL? {
    .init(string: "https://api.xerinola.io")
  }
}
