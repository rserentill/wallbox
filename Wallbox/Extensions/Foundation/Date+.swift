//
//  Date+.swift
//  Wallbox
//
//  Created by Roger Serentill Gené on 18/9/22.
//

import Foundation

extension Date {
  static func -(lhs: Self, rhs: Self) -> TimeInterval {
    lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
  }
  
  var short: String {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd/MM HH:mm"
    return formatter.string(from: self)
  }
}
