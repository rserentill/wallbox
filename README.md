# Wallbox iOS app

This is the result of the technical test for the Senior iOS position.

## App Architecture

I have used Clean Architecture, to make a clear separation for presentation, domain and data layers.

![Clean Architecture](Images/Clean Architecture.png)



## Design Pattern

The different modules of the app follow **MVVM-C** pattern.

* **Model**: The model layer holds the data needed for each module. 
* **View**: The view renders the UI components and handles user interactions.
* **ViewModel**: The view model is responsible for the presentation logic, tells the view what information to present to the user and communicates with the domain layer.
* **Coordinator**: Each module has a coordinator, whose main responsibility is app navigation. It instantiates each component and its dependencies.

For the business logic I used Interactors, which comunicate to the data layer through services to retrieve data from the network. 

![MVVM-C](Images/MVVM-C.png)



## Pending TODOs

* Adaptative design for iPad
* Light and dark mode colors, although the app is already configured with color sets and ready to be configured with both modes.
* `DetailViewModel` tests. 